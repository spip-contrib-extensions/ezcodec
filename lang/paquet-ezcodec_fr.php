<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
return [
	// E
	'ezcodec_description' => 'Framework de gestion de l\'encodage et du décodage de contenus. Ce plugin fournit une API générique pour encoder et décoder des contenus et des encodeurs et décodeurs YAML, JSON, XML et CVS. Il est aussi possible d\'étendre la liste des formats supportés.',
	'ezcodec_slogan' => 'La fabrique pour encoder & décoder vos contenus'
];
