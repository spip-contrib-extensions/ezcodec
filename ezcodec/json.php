<?php
/**
 * Ce fichier contient les fonctions de service pour l'encodage et le décodage JSON.
 *
 * @package SPIP\EZCODEC\JSON
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Encode un tableau en une chaine formatée en JSON.
 *
 * @param array      $contenu Tableau brut à traduire en chaine JSON
 * @param null|array $options Options d'encodage JSON :
 *                            - `flags` : index permettant de fournir les flags JSON
 *                            - `depth` : index pour la profondeur maximale de récursivité
 *
 * @return string Chaine formatée en JSON ou vide en cas d'erreur
**/
function contenu_encoder_json_dist(array $contenu, ?array $options = []) : string {
	// Initialisation de la chaine de sortie
	$contenu_encode = '';

	if ($contenu) {
		// On initialise les options avec celles par défaut
		$options_defaut = [
			'flags' => JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRESERVE_ZERO_FRACTION,
			'depth' => 512,
		];
		// -- profondeur max
		$depth = $options['depth'] ?? $options_defaut['depth'];
		// -- les flags : on exclut toujours le flag JSON_THROW_ON_ERROR de façon à ne pas générer d'exception mais de gérer l'erreur globale
		$flags = $options['flags'] ?? $options_defaut['flags'];
		$flags &= ~JSON_THROW_ON_ERROR;

		// Appel de l'encodage JSON PHP en testant le retour et pas une exception
		$contenu_encode = json_encode($contenu, $flags, $depth);
		if (
			($erreur = json_last_error())
			and $erreur !== JSON_ERROR_NONE
			and ($contenu_encode === false)
		) {
			$contenu_encode = '';
			$message = json_last_error_msg();
			spip_log("Erreur $erreur lors de l'encodage JSON ($message)", 'ezcodec' . _LOG_ERREUR);
		}
	}

	return $contenu_encode;
}

/**
 * Décode, une chaine formatée en JSON, en un tableau.
 * De fait, l'option de tableau associatif est toujours à `true`.
 *
 * @param string     $contenu Contenu brut issu d'un fichier XML
 * @param null|array $options Options d'encodage JSON :
 *                            - `flags` : index permettant de fournir les flags JSON
 *                            - `depth` : index pour la profondeur maximale de récursivité
 *
 * @return array Tableau associatif issu du décodage ou vide sinon
**/
function contenu_decoder_json_dist(string $contenu, ?array $options = []) : array {
	$contenu_decode = [];

	if ($contenu) {
		// On initialise les options avec celles par défaut
		$options_defaut = [
			'flags' => 0,
			'depth' => 512,
		];
		// -- profondeur max
		$depth = $options['depth'] ?? $options_defaut['depth'];
		// -- les flags : on exclut toujours le flag JSON_THROW_ON_ERROR de façon à ne pas générer d'exception mais de gérer l'erreur globale
		$flags = $options['flags'] ?? $options_defaut['flags'];
		$flags &= ~JSON_THROW_ON_ERROR;

		// Appel du décodage JSON PHP en testant le retour et pas une exception
		$contenu_decode = json_decode($contenu, true, $depth, $flags);
		if (
			($erreur = json_last_error())
			and $erreur !== JSON_ERROR_NONE
		) {
			$contenu_decode = [];
			$message = json_last_error_msg();
			spip_log("Erreur $erreur lors de l'encodage JSON ($message)", 'ezcodec' . _LOG_ERREUR);
		}
	}

	return $contenu_decode;
}
