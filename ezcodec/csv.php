<?php
/**
 * Ce fichier contient les fonctions de service pour l'encodage et le décodage CSV.
 *
 * @package SPIP\EZCODEC\CSV
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Encode un tableau en une chaine formatée en CSV.
 *
 * Cette fonction est inspirée de la fonction d'importation CSV de spip dont elle utilise certaines routines natives.
 * Par contre, par défaut, on s'attend, si besoin, ici à des en-têtes en première ligne du tableau.
 *
 * @param array      $contenu Tableau brut à traduire en chaine CSV
 * @param null|array $options Options d'encodage CSV :
 *                            - `entetes`  : `true` si la première ligne du contenu définit les noms de colonnes, `false` sinon
 *                            - `delim`    : caractère de séparation des champs. Prend les valeurs `,` (défaut), `;`, `\t` ou `TAB`
 *                            - `charset`  : charset de la chaine encodée si different de celui du site
 *                            - `callback` : fonction à appeler sur chaque ligne pour mettre en forme voire completer les donnees
 *
 * @return string Chaine formatée en CSV ou vide sinon
**/
function contenu_encoder_csv_dist(array $contenu, ?array $options = []) : string {
	// Initialisation de la chaine de sortie
	$contenu_encode = '';

	// Traduire le tableau associatif fourni en un tableau de l'en-tête et un autre des données.
	if ($contenu) {
		// Comme les fonctions natives sur les CSV passent toujours par la création d'un fichier, on crée un fichier
		// temporaire dont la chaine sera extraite à la fin.
		$ressource_tmp = fopen('php://temp/maxmemory:0', 'w+');

		// On initialise les options avec celles par défaut
		$options_defaut = [
			'entetes'  => true,
			'delim'    => ',',
			'charset'  => null,
			'callback' => '',
		];
		$options = array_merge($options_defaut, $options);

		// Charset : celui indiqué en option, sinon celui du site
		$charset_site = $GLOBALS['meta']['charset'];
		$charset = $options['charset'] ?? $charset_site;
		$importer_charset = (($charset === $charset_site) ? null : $charset);

		// On traite l'en-tête à partir des index du tableau si demandé
		include_spip('inc/exporter_csv');
		$nb = 0;
		if ($options['entetes']) {
			$entetes = array_keys($contenu[0]);
			$contenu_tmp = exporter_csv_ligne_numerotee($nb, $entetes, $options['delim'], $importer_charset, $options['callback']);
			fwrite($ressource_tmp, $contenu_tmp);
		}

		// On traite les lignes de données
		// -- les donnees commencent toujours a la ligne 1, en-têtes ou pas
		$nb++;
		foreach ($contenu as $_ligne) {
			$ligne = array_values($_ligne);
			$contenu_tmp = exporter_csv_ligne_numerotee($nb, $ligne, $options['delim'], $importer_charset, $options['callback']);
			fwrite($ressource_tmp, $contenu_tmp);
			$nb++;
		}

		// On récupère juste la chaine et on clot la ressource
		rewind($ressource_tmp);
		$contenu_encode = stream_get_contents($ressource_tmp);
		fclose($ressource_tmp);
	}

	return $contenu_encode;
}

/**
 * Décode, suivant le délimiteur précisé, une chaine formatée comme un contenu CSV, en un tableau dont les champs
 * correspondent aux noms des colonnes du CSV.
 *
 * Cette fonction est inspirée de celle de spip qui crée le fichier dans la foulée.
 * Pour plus de cohérence, on garde le même nom pour les options que pour l'encodage si cela a du sens.
 *
 * @param string     $contenu Contenu brut issu d'un fichier CSV
 * @param null|array $options Options d'encodage YAML :
 *                            - `entetes` : `true` si la première ligne du contenu définit les noms de colonnes, `false` sinon
 *                            - `delim`   : caractère de séparation des champs. Prend les valeurs `,` (défaut), `;`, `\t` ou `TAB`
 *                            - `enclos`  : caractère d'encadrement de texte (un caractère d'un seul octet).
 *                            - `len`     : taille maximale d'une ligne
 *                            - `charset` : charset source du contenu si différent de utf-8 ou iso-8859-1
 *
 * @return array Tableau associatif issu du décodage ou tableau vide sinon
**/
function contenu_decoder_csv_dist(string $contenu, ?array $options = []) : array {
	// Pour utiliser simplement la fonction de spip, on écrit un fichier CSV temporaire à partir du contenu
	// et on appelle la fonction sur ce fichier
	// -- création du fichier temporaire
	include_spip('inc/flock');
	$dir_tmp = sous_repertoire(_DIR_TMP, 'ezcodec');
	$fichier_tmp = $dir_tmp . 'csv_' . date('Ymd-His');
	ecrire_fichier($fichier_tmp, $contenu);

	// On initialise les options avec celles par défaut
	$options_defaut = [
		'entetes' => true,
		'delim'   => ',',
		'enclos'  => '"',
		'len'     => 10000,
		'charset' => null,
	];
	$options = array_merge($options_defaut, $options);
	// -- on transfert entetes vers head qui est l'index utilisé par spip et charset_source vers charset
	$options['head'] = $options['entetes'];
	$options['charset_source'] = $options['charset'] ?? '';

	// Appel de la fonction spip
	$importer = charger_fonction('importer_csv', 'inc');
	$contenu_decode = $importer($fichier_tmp, $options);

	// Suppression du fichier temporaire
	supprimer_fichier($fichier_tmp);

	return ($contenu_decode !== false ? $contenu_decode : []);
}
