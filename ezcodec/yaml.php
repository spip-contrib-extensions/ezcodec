<?php
/**
 * Ce fichier contient les fonctions de service pour l'encodage et le décodage YAML.
 *
 * @package SPIP\EZCODEC\YAML
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Encode un tableau en une chaine formatée en YAML.
 * On utilise la librairie Symfony de façon à avoir le maximum de flexibilité dans les options.
 *
 * @param array      $contenu Tableau brut à traduire en chaine YAML
 * @param null|array $options Options d'encodage YAML :
 *                            - `flags`  : index permettant de fournir les flags YAML de la librairie symfony (_DUMP_xxx)
 *                            - `indent` : nombre d'espaces pour chaque niveau d'indentation, 2 par défaut.
 *                            - `inline` : niveau à partir duquel la présentation du YAML devient inline, 3 par défaut.
 *
 * @return string Chaine formatée en YAML ou vide en cas d'erreur
**/
function contenu_encoder_yaml_dist(array $contenu, ?array $options = []) : string {
	// Initialisation de la chaine de sortie
	$contenu_encode = '';

	if (
		defined('_DIR_PLUGIN_YAML')
		and $contenu
	) {
		// On initialise les options avec celles par défaut
		// -- indentation, inline et flags
		$options_defaut = [
			'flags'  => 0,
			'indent' => 2,
			'inline' => 3,
		];
		$options = array_merge($options_defaut, $options);
		// -- librairie : on force symfony
		$options['library'] = 'symfony';

		// Appel de l'encodage YAML PHP en testant le retour et pas une exception
		include_spip('inc/yaml');
		$contenu_encode = yaml_encode($contenu, $options);
		if ($contenu_encode === false) {
			$contenu_encode = '';
			spip_log("Erreur lors de l'encodage YAML", 'ezcodec' . _LOG_ERREUR);
		}
	}

	return $contenu_encode;
}

/**
 * Décode, une chaine formatée en YAML, en un tableau.
 * De fait, l'option de tableau associatif est toujours à `true`.
 *
 * @param string     $contenu Contenu brut issu d'un fichier XML
 * @param null|array $options Options d'encodage YAML :
 *                            - `flags` : index permettant de fournir les flags YAML
 *                            - `show_error` : index pour afficher les erreurs de décodage
 *
 * @return array Tableau associatif issu du décodage ou vide sinon
**/
function contenu_decoder_yaml_dist(string $contenu, ?array $options = []) : array {
	$contenu_decode = [];

	if (
		defined('_DIR_PLUGIN_YAML')
		and $contenu
	) {
		// On initialise les options avec celles par défaut
		// -- indentation, inline et flags
		$options_defaut = [
			'flags'      => 0,
			'show_error' => false,
		];
		$options = array_merge($options_defaut, $options);
		// -- librairie : on force symfony
		$options['library'] = 'symfony';

		// Appel du décodage YAML en testant le retour et pas une exception
		include_spip('inc/yaml');
		$contenu_decode = yaml_decode($contenu, $options);
		if (!is_array($contenu_decode)) {
			$contenu_decode = [];
			spip_log('Erreur lors du décodage YAML', 'ezcodec' . _LOG_ERREUR);
		}
	}

	return $contenu_decode;
}
