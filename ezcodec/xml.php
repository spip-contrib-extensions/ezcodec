<?php
/**
 * Ce fichier contient les fonctions de service pour l'encodage et le décodage XML.
 *
 * @package SPIP\EZCODEC\XML
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Encode, un tableau en une chaine formatée en XML.
 * Cette fonction utilise SimpleXML et ajoute une racine dont le nom est soit fournie par l'appelant
 * soit forcé à `<ezcodec>`. Si l'appelant passe une chaine vide, aucune balise englobante ne sera ajoutée.
 *
 * @see https://www.techieclues.com/blogs/convert-array-to-xml-document-in-php
 *
 * @param array      $contenu Tableau brut à traduire en chaine XML
 * @param null|array $options Options d'encodage XML :
 *                            - `root`      : précise le nom d'une balise englobant le contenu ou vide pour ne pas encapsuler le contenu
 *                            - `flags`     : index permettant de fournir les flags libxml
 *                            - `is_url`    : le contenu est un chemin d'accès ou un URL pointant à un document XML au lieu d'une chaîne de caractères de données
 *                            - `ns_prefix` : préfixe ou URI de l'espace de noms.
 *                            - `is_prefix` : true si ns_prefix est un préfixe, false si c'est une URI.
 *
 * @return string Chaine formatée en XML ou vide sinon
**/
function contenu_encoder_xml_dist(array $contenu, ?array $options = []) : string {
	// On initialise les options avec celles par défaut
	$options_defaut = [
		'root'      => 'ezcodec',
		'flags'     => LIBXML_NOXMLDECL,
		'is_url'    => false,
		'ns_prefix' => '',
		'is_prefix' => false
	];
	$options = array_merge($options_defaut, $options);

	// Encodage du tableau de contenu
	return ezcodec_encoder_xml($contenu, $options);
}

/**
 * Décode, une chaine formatée comme un contenu XML, en un tableau.
 * Le chaine a forcément été encodée par la fonction inverse `ezcache_encoder_xml()`.
 *
 * @param string     $contenu Contenu brut issu d'un fichier XML
 * @param null|array $options Options d'encodage XML :
 *                            - `with_root` : true si on veut aussi la balise root du contenu, false sinon (défaut))
 *                            - `flags`     : index permettant de fournir les flags libxml
 *                            - `class`     : permet de fournir en sortie du décodage un objet de la classe spécifiée qui implémente SimpleXMLElement
 *                            - `ns_prefix` : préfixe ou URI de l'espace de noms.
 *                            - `is_prefix` : true si ns_prefix est un préfixe, false si c'est une URI.
 *
 * @return array Tableau associatif issu du décodage ou tableau vide sinon
**/
function contenu_decoder_xml_dist(string $contenu, ?array $options = []) : array {
	// On initialise les options avec celles par défaut
	$options_defaut = [
		'with_root' => false,
		'flags'     => LIBXML_NOEMPTYTAG,
		'class'     => null,
		'ns_prefix' => '',
		'is_prefix' => false
	];
	$options = array_merge($options_defaut, $options);

	// Si on veut la balise racine du contenu XML, il faut l'encapsuler dans une balise englobante
	// -- pour ce faire, il faut supprimer le doctype et la DTD éventuellement insérés en début de contenu.
	if ($options['with_root']) {
		// On supprime DTD et Doctype
		$patterns = [
			',<\?xml\s+.*\?>,i',
			',<!DOCTYPE\s+(\w+)\s+(\w+)\s*([^>]*)>\s*,i'
		];
		$contenu = preg_replace($patterns, '', $contenu);
		// On englobe le contenu dans la balise racine
		$contenu = "<ezcodec>{$contenu}</ezcodec>";
	}

	$contenu_decode = [];
	$xml_decode = simplexml_load_string(
		$contenu,
		$options['class'],
		$options['flags'],
		$options['ns_prefix'],
		$options['is_prefix']
	);
	if ($xml_decode !== false) {
		$contenu_decode = json_decode(json_encode($xml_decode), true);
	} else {
		$message = '';
		$erreurs = libxml_get_errors();
		foreach ($erreurs as $_erreur) {
			$message .= ($message ? ' / ' : '') . 'ligne ' . $_erreur->line . ' - colonne ' . $_erreur->column . ' - code ' . $_erreur->code;
		}
		libxml_clear_errors();
		spip_log("Erreur lors du décodage XML ({$message})", 'ezcodec' . _LOG_ERREUR);
	}

	return $contenu_decode ?: [];
}

// --------------------------------------------------------------
// ----------------- UTILITAIRES INTERNES XML -------------------
// --------------------------------------------------------------

/**
 * Encode, un tableau en une chaine formatée en XML.
 * Cette fonction utilise SimpleXML et ajoute toujours une racine.
 *
 * @see https://www.techieclues.com/blogs/convert-array-to-xml-document-in-php
 *
 * @param array       $contenu Tableau brut à traduire en chaine XML
 * @param null|array  $options Options d'encodage XML (voir la fonction appelante)
 * @param null|object $xml     Objet XML en cours de construction
 *
 * @return string Chaine formatée en XML ou vide sinon
**/
function ezcodec_encoder_xml(array $contenu, ?array $options, ?object $xml = null) : string {
	if ($xml === null) {
		$root = $options['root'] ? "<{$options['root']}></{$options['root']}>" : '';
		$xml = new SimpleXMLElement(
			$root,
			$options['flags'],
			$options['is_url'],
			$options['ns_prefix'],
			$options['is_prefix']
		);
	}

	foreach ($contenu as $_cle => $_valeur) {
		if (is_array($_valeur)) {
			ezcodec_encoder_xml($_valeur, $options, $xml->addChild($_cle));
		} else {
			$xml->addChild($_cle, htmlspecialchars($_valeur));
		}
	}

	return $xml->asXML();
}
