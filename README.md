# Encoder Factory

![ezcodec](ezcodec.svg)

Framework de gestion de l'encodage et du décodage de contenus.

Ce plugin fournit une API générique pour encoder et décoder des contenus et des encodeurs et décodeurs YAML, JSON, XML
et CVS. Il est aussi possible d'étendre les formats supportés ou de surcharger les formats déjà proposés.

Dans sa composition actuelle, le plugin supporte l'encodage et le décodage des formats :
- JSON
- YAML
- XML
- et CSV.

Les fonctions spécifiques à un format donné peuvent être surchargées et de nouveau format peuvent être ajoutés en suivant
l'organisation api-services.
