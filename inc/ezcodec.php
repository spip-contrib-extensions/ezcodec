<?php
/**
 * Ce fichier contient les fonctions d'API du plugin Encoder Factory.
 *
 * @package SPIP\EZCODE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Encode un contenu dans un format donné et renvoie la chaine encodée.
 *
 * @api
 *
 * @param array      $contenu Contenu à encoder fourni au format tableau.
 * @param string     $format  Identifiant du format dans lequel encoder le contenu
 * @param null|array $options Options d'encodage dont la liste dépend du format
 *
 * @return string Contenu encodé ou chaine vide en cas d'erreur.
 */
function contenu_encoder(array $contenu, string $format, ?array $options = []) : string {
	// Initialisation du retour de la fonction
	$contenu_encode = '';

	// Recherche de la fonction d'encodage à partir du format.
	// - si la fonction n'existe pas on renvoie une chaine vide et on loge l'erreur
	if (
		include_spip("ezcodec/{$format}")
		and (
			(
				($encoder = "contenu_encoder_{$format}")
				and function_exists($encoder)
			)
			or (
				($encoder = "contenu_encoder_{$format}_dist")
				and function_exists($encoder)
			)
		)
	) {
		$contenu_encode = $encoder($contenu, $options);
	} else {
		spip_log("Le format {$format} ne possède pas de fonction d'encodage", 'ezcodec' . _LOG_ERREUR);
	}

	return $contenu_encode;
}

/**
 * Décode un contenu au format donné et renvoie le tableau décodé.
 *
 * @api
 *
 * @param string     $contenu Contenu à décoder fourni au format chaine.
 * @param string     $format  Identifiant du format dans lequel est encodé le contenu
 * @param null|array $options Options de décodage dont la liste dépend du format
 *
 * @return array Contenu décodé ou tableau vide en cas d'erreur.
 */
function contenu_decoder(string $contenu, string $format, ?array $options = []) : array {
	// Initialisation du retour de la fonction
	$contenu_decode = [];

	// Recherche de la fonction de décodage à partir du format.
	// - si la fonction n'existe pas on renvoie une chaine vide et on loge l'erreur
	if (
		include_spip("ezcodec/{$format}")
		and (
			(
				($decoder = "contenu_decoder_{$format}")
				and function_exists($decoder)
			)
			or (
				($decoder = "contenu_decoder_{$format}_dist")
				and function_exists($decoder)
			)
		)
	) {
		$contenu_decode = $decoder($contenu, $options);
	} else {
		spip_log("Le format {$format} ne possède pas de fonction de décodage", 'ezcodec' . _LOG_ERREUR);
	}

	return $contenu_decode;
}
